<?php
    $cabecera= file_get_contents("template/cabecera.html");
    echo $cabecera;
?>
  <body>
      <div class="container">
    <h1>Ejemplo</h1>
    <form method="get">
    
    <div class="form-group">
    <label for="color">Color de la linea</label>
    <select class="form-control" id="color">
        <option value="RED">Rojo</option>
        <option value="BLUE">Azul</option>
        <option value="BLACK">Negro</option>
    </select>
    
    <fieldset class="form-group">
        <legend>Dibujar punto</legend>
    <label for="puntoX">
        Introduce la coordenada X
    </label>
    <input class="form-control" type="text" id="puntoX" name="px">
    <label for="puntoY">
        Introduce la coordenada Y
    </label>
    <input class="form-control" type="text" id="puntoY" name="py">
    </fieldset>
  </div>
        </form>
    </div>
<?php
    $pie= file_get_contents("template/pie.html");
    echo $pie;
?>
  </body>
</html>